4. Gerenciamento de Licenças de Software
Problema:Um software de design gráfico precisa garantir que apenas um número limitado de usuários possa utilizá-lo simultaneamente, conforme as licenças adquiridas pela empresa.
Solução com Proxy:Criar um Proxy de Gerenciamento de Licenças que gerencia as sessões ativas do software. Quando um usuário tenta iniciar o software, o proxy verifica se uma licença está disponível. Se todas as licenças estiverem em uso, o proxy pode colocar a solicitação em uma fila de espera ou negar o acesso, garantindo o cumprimento das restrições de licença.

![image.png](image.png)
![teste.png](teste.png)