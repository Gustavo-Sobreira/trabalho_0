4. Personalização de Interfaces de Usuário
Problema: Um aplicativo precisa oferecer diferentes temas e configurações de interface para atender às preferências de seus usuários, como modo escuro/claro, tamanhos de fonte ajustáveis e cores personalizadas.

Solução com Decorator: Usar decoradores para modificar dinamicamente a aparência dos componentes da interface do usuário. Cada decorador pode adicionar uma camada de personalização, como alterar o esquema de cores ou ajustar o tamanho da fonte, permitindo que os usuários configurem a interface de acordo com suas preferências.

![image.png](image.png)
![teste.png](teste.png)
