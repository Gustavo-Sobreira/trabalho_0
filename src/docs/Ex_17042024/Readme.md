4. Sistema de Logística
Problema: Uma empresa de logística opera com diferentes modos de transporte (rodoviário, marítimo, aéreo). Cada modo de transporte tem suas próprias características e algoritmos de cálculo de custo. O sistema precisa ser capaz de planejar e estimar o custo do transporte de mercadorias com base no modo de transporte selecionado.

Solução com Factory Method: Usar o padrão Factory Method para criar uma classe abstrata de fábrica de transporte com um método que retorna um objeto de transporte. Subclasses concretas da fábrica instanciam os diferentes modos de transporte. Isso permite ao sistema calcular o custo de forma dinâmica, baseando-se no tipo de transporte escolhido, promovendo a flexibilidade e a eficiência.

![image.png](image.png)
![teste.png](teste.png)