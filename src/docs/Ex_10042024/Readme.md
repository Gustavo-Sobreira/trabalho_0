# Aplicador de Filtros de Imagem

Este é um exemplo simples de um aplicador de filtros de imagem usando o padrão de projeto Factory.

## Descrição

O projeto consiste em várias classes:

- `FilterFactory`: Uma fábrica para obter filtros de acordo com o tipo fornecido.
- `Filtro`: Uma interface para os diferentes tipos de filtros que podem ser aplicados.
- `GaussianBlurFilter`: Uma implementação de filtro para aplicar o efeito de desfoque gaussiano.
- `SepiaFilter`: Uma implementação de filtro para aplicar o efeito de sepia.
- `Imagem`: Uma classe que representa uma imagem e pode aplicar um filtro a ela.

## Funcionamento

A classe `FilterFactory` contém um mapa de diferentes tipos de filtros disponíveis. Quando solicitado, ele retorna a instância apropriada do filtro com base no tipo fornecido. Se o filtro correspondente não estiver presente no mapa, ele é criado e armazenado no mapa para uso futuro.

A classe `Imagem` é responsável por aplicar um filtro a uma imagem. Ela obtém o filtro necessário da `FilterFactory` e o aplica à imagem.

## Como Utilizar

1. Crie uma instância da classe `Imagem`, fornecendo o nome do arquivo da imagem como argumento para o construtor.
2. Chame o método `aplicarFiltro()` da instância criada, passando o tipo de filtro desejado como argumento.

![Diagrama de classes.PNG](Diagrama.png)
![img.png](img.png)
