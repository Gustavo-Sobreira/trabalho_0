# Logger de Aplicação:

Problema: Em muitas aplicações, é crucial ter um sistema de logging para registrar eventos, erros e informações de debug. Ter múltiplas instâncias de loggers pode resultar em logs fragmentados ou em conflitos no acesso aos recursos de logging.
Solução Singleton: Um Singleton para o sistema de logging garante um ponto centralizado para registrar logs, facilitando a manutenção, a consistência e a acessibilidade dos registros de log.


![img.png](Diagrama.png)
![img.png](img.png)