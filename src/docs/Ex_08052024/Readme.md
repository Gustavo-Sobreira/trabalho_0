# Padrão Interpreter

Problema: Interpretar e executar comandos fornecidos por usuários através de uma CLI, onde os comandos podem ter diferentes argumentos e opções.

Solução com Interpreter: Implementar um interpretador que analisa a entrada do usuário (comando e argumentos), valida essa entrada com base em uma gramática definida e executa as ações correspondentes.

![img.png](teste.png)
![img.png](img.png)****