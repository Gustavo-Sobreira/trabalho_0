4. Configurações de Aplicações com Funcionalidade de Restaurar Padrões

Problema: Em aplicações que permitem aos usuários personalizar configurações (como preferências de usuário, temas, atalhos, etc.), pode ser necessário restaurar as configurações para um estado anterior ou para os padrões de fábrica.

Solução com Memento: Utilizar o padrão Memento para capturar o estado das configurações após cada alteração significativa. O Originator seria o objeto que representa as configurações, o Memento armazenaria o estado atual das configurações, e o GerenciadorEstados gerenciaria os estados salvos.


![image.png](image.png)
![teste.png](teste.png)