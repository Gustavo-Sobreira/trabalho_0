package Ex_08052024;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class InterpretadorLinhaComandoWin implements IInterpretador {
    String retorno = "";
    String diretorioAtual = "Área_de_Trabalho Conhecimentos Downloads Faculdade Profissional snap Certificados Cursos eBooks Imagens senhas TCC Documentos Estudos Monitoria";
    List<String> diretorios = Arrays.asList(diretorioAtual.split(" "));

    InterpretadorLinhaComandoWin(List<String> parametros){
        Iterator<String> stringIterator = parametros.iterator();
        while (stringIterator.hasNext()){
            String parametro = stringIterator.next();
            if (parametro.equals("dir")){
                retorno = diretorioAtual;
            } else if (parametro.equals("cd")) {
                if (!stringIterator.hasNext()) {
                    retorno = "Diretório esperado após o comando 'cd'";
                    return;
                }
                String novoDiretorio = stringIterator.next();
                if (!novoDiretorio.isEmpty() && diretorios.contains(novoDiretorio)) {
                    retorno = "Navegando para o diretório: " + novoDiretorio;
                } else {
                    retorno = "Diretório '" + novoDiretorio + "' não encontrado";
                    return;
                }
            }else {
                retorno = "Comando não encontrado";
            }
        }

    };

    @Override
    public String resposta() {
        return retorno;
    }
}
