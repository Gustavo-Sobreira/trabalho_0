package Ex_08052024;

import java.util.Arrays;
import java.util.List;

public class InterpretadorLinhaDeComando implements IInterpretador {

    String retornoAoUsuario = "";

    InterpretadorLinhaDeComando(String sitema, String comando){
        List<String> parametros = Arrays.asList(comando.split(" "));

        switch (sitema){
            case "$":
                if (comando.isEmpty()){
                    throw new IllegalArgumentException("comando não encontrado");
                }
                retornoAoUsuario = new InterpretadorLinhaComandoLinux(parametros).resposta();
                break;
            case ">_":
                if (comando.isEmpty()){
                    throw new IllegalArgumentException("comando não encontrado");
                }
                retornoAoUsuario = new InterpretadorLinhaComandoMac(parametros).resposta();
                break;
            case "/":
                if (comando.isEmpty()){
                    throw new IllegalArgumentException("comando não encontrado");
                }
                retornoAoUsuario = new InterpretadorLinhaComandoWin(parametros).resposta();
                break;
            default:
                retornoAoUsuario = "Sistema não suportado";
        }

    }
    @Override
    public String resposta() {
        return retornoAoUsuario;
    }
}
