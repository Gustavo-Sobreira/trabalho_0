package Ex_08052024;

public class MaquinaLinux implements IMaquina {
    String inicializador = "$";

    public String receberComando(String comando) {
        String respostaAoComando;
        respostaAoComando = new InterpretadorLinhaDeComando(
                inicializador,
                comando
        ).resposta();

        return respostaAoComando;
    }
}
