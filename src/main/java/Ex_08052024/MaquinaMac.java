package Ex_08052024;

public class MaquinaMac implements IMaquina {
    String inicializador = ">_";

    public String receberComando(String comando) {
        String respostaAoComando;
        respostaAoComando = new InterpretadorLinhaDeComando(
                inicializador,
                comando
        ).resposta();

        return respostaAoComando;
    }
}
