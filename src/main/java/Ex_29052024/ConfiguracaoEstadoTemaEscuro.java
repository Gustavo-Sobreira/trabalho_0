package Ex_29052024;

public class ConfiguracaoEstadoTemaEscuro implements IConfiguracaoEstado {
    private ConfiguracaoEstadoTemaEscuro() {}

    private static ConfiguracaoEstadoTemaEscuro instance = new ConfiguracaoEstadoTemaEscuro();

    public static ConfiguracaoEstadoTemaEscuro getInstance() {
        return instance;
    }

    @Override
    public String getConfiguracaoAtual() {
        return "Tema: Claro";
    }
}
