package Ex_29052024;

import java.util.ArrayList;
import java.util.List;

public class Configuracao {
    private IConfiguracaoEstado configuracao;
    private List<IConfiguracaoEstado> memento = new ArrayList<IConfiguracaoEstado>();

    public IConfiguracaoEstado getConfiguracao() {
        return this.configuracao;
    }

    public void setConfiguracao(IConfiguracaoEstado configuracao) {
        this.configuracao = configuracao;
        this.memento.add(this.configuracao);
    }

    public void resetarConfiguracao(){
        this.configuracao = this.memento.get(0);
    }

    public void voltarEstadoAnterior(int indice) {
        if (indice < 0 || indice > this.memento.size() - 1) {
            throw new IllegalArgumentException("[ERROR] - Nenhuma configuração encontrada!");
        }
        this.configuracao = this.memento.get(indice);
    }

    public List<IConfiguracaoEstado> getEstados() {
        return this.memento;
    }
}
