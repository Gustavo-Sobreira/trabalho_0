package Ex_29052024;

public class ConfiguracaoEstadoTemaClaro implements IConfiguracaoEstado {
    private ConfiguracaoEstadoTemaClaro() {}

    private static ConfiguracaoEstadoTemaClaro instance = new ConfiguracaoEstadoTemaClaro();

    public static ConfiguracaoEstadoTemaClaro getInstance() {
        return instance;
    }

    @Override
    public String getConfiguracaoAtual() {
        return "Tema: Claro";
    }
}
