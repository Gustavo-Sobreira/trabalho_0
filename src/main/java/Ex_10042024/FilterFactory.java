package Ex_10042024;

import java.util.HashMap;
import java.util.Map;

public class FilterFactory {
    private static final Map<String, Filtro> filtros = new HashMap<>();

    public static Filtro getFiltro(String type) {
        Filtro filtro = filtros.get(type);

        if (filtro == null) {
            switch (type) {
                case "gaussianBlur":
                    filtro = new GaussianBlurFilter();
                    break;
                case "sepia":
                    filtro = new SepiaFilter();
                    break;
            }
            filtros.put(type, filtro);
        }

        return filtro;
    }
}
