package Ex_10042024;

public class SepiaFilter implements Filtro {
    @Override
    public void apply(String nomeArquivo) {
        System.out.println("Aplicando Sepia filter para imagem: " + nomeArquivo);
    }
}
