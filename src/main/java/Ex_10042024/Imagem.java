package Ex_10042024;

public class Imagem {
    private String nomeArquivo;

    public Imagem(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public void aplicarFiltro(String tipoFiltro) {
        Filtro filtro = FilterFactory.getFiltro(tipoFiltro);
        filtro.apply(nomeArquivo);
    }
}
