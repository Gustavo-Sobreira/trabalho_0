package Ex_17042024;

public class TransporteAereo implements ITransporte {
    @Override
    public double calcularCusto(double distancia) {
        return distancia * 0.2;
    }
}
