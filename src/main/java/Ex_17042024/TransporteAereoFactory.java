package Ex_17042024;

public class TransporteAereoFactory extends TransporteFactory {
    @Override
    public ITransporte criarTransporte() {
        return new TransporteAereo();
    }
}
