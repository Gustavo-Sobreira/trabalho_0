package Ex_17042024;

public class TransporteAquaticoFactory extends TransporteFactory {
    @Override
    public ITransporte criarTransporte() {
        return new TransporteAquatico();
    }
}
