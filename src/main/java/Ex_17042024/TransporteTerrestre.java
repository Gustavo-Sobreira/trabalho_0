package Ex_17042024;

public class TransporteTerrestre implements ITransporte {
    @Override
    public double calcularCusto(double distancia) {
        return distancia * 0.1;
    }
}
