package Ex_17042024;

public class TransporteTerrestreFactory extends TransporteFactory {
    @Override
    public ITransporte criarTransporte() {
        return new TransporteTerrestre();
    }
}
