    package Ex_23042024;

    import java.io.*;
    import java.text.SimpleDateFormat;
    import java.util.Date;

    public class Logger {

        private PrintWriter printWriter;
        private static Logger instance;
        private final SimpleDateFormat data;

        public Logger(){
            try {
                printWriter = new PrintWriter(new FileWriter("trabalho0padrao_singleton.log", true));
            } catch (IOException e) {
                e.printStackTrace();
            }
            data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }

        public void log(String message) {
            String timestamp = data.format(new Date());
            printWriter.println(timestamp + " - " + message);
            printWriter.flush();
        }

        public static Logger getInstance() {
            if (instance == null){
                instance = new Logger();
            }
            return instance;
        }

        public synchronized boolean fecharLogger() {
            printWriter.close();
            return true;
        }

        public String ultimoLog(String filePath) throws IOException {
            try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
                String lina = "", ultimaLinha = "";
                while ((lina = reader.readLine()) != null) {
                    ultimaLinha = lina;
                }
                return ultimaLinha;
            }
        }

        public Object destroyer() {
            if (printWriter != null) {
                printWriter.close();
            }
            instance = null;
            return instance;
        }
    }





