package Ex_09042024;

public class Software implements ISoftware {

    private String nome;
    private Integer codigoSerie;

    public Software(Integer codigoSerie){
        this.codigoSerie = codigoSerie;
        Software software = BD.getSoftware(codigoSerie);
        this.nome = software.nome;
    }

    public Software(Integer codigoSerie, String nome){
        this.nome = nome;
        this.codigoSerie = codigoSerie;
    }

    public Integer getCodigoSerie(){
        return getCodigoSerie();
    }

    @Override
    public String interagir(Acesso acesso) {
        return "Usuário " + acesso.getIpUsuario() + " interagindo com " + nome;
    }
}