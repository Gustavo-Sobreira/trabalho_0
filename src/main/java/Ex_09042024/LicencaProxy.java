package Ex_09042024;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class LicencaProxy implements ISoftware {
    private Software software;
    private Acesso acesso;
    private Queue<Acesso> filaDeAcesso = new LinkedList<>();

    public LicencaProxy(Software software){
        this.software = software;
    }

    private boolean gerenciarAcesso(){
        filaDeAcesso.add(this.acesso);
        if (filaDeAcesso.size() <= acesso.getEmpresa().getTotalDeAcesso()){
            return true;
        }
        return false;
    }

    public void liberarAcesso(Acesso acesso){
        filaDeAcesso.remove(acesso);
    }

    public String listarAcessos(){
        return filaDeAcesso.stream().limit(acesso.getEmpresa().getTotalDeAcesso()).collect(Collectors.toList()).toString();
    }

    @Override
    public String interagir(Acesso acesso) {
        this.acesso = acesso;
        if (gerenciarAcesso()){
            if (this.software == null){
                this.software = new Software(this.acesso.getCodigoProduto());
            }
            return this.software.interagir(acesso);
        }
        return "[OPS] - Você está na fila de acesso, aguarde ou compre mais licenças";
    }
}
