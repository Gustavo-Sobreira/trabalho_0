package Ex_09042024;

public class Empresa {
    private Integer totalDeAcesso;

    public Empresa(Integer totalDeAcesso) {
        this.totalDeAcesso = totalDeAcesso;
    }

    public Integer getTotalDeAcesso() {
        return totalDeAcesso;
    }

    public void setTotalDeAcesso(Integer totalDeAcesso) {
        this.totalDeAcesso = totalDeAcesso;
    }
}
