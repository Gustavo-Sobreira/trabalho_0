package Ex_09042024;

import java.util.HashMap;
import java.util.Map;

public class BD {
    private static Map<Integer, Software> softwareMap = new HashMap<>();
    public static Software getSoftware(Integer codigoSerie) {
        return softwareMap.get(codigoSerie);
    }

    public static void addSoftware(Software software) {
        softwareMap.put(software.getCodigoSerie(), software);
    }
}
