package Ex_09042024;

public class Acesso {
    private Integer codigoProduto;
    private Integer ipUsuario;
    private Empresa empresa;

    public Acesso(Integer codigoProduto, Integer ipUsuario, Empresa empresa) {
        this.codigoProduto = codigoProduto;
        this.ipUsuario = ipUsuario;
        this.empresa = empresa;
    }

    public Integer getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(Integer codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public Integer getIpUsuario() {
        return ipUsuario;
    }

    public void setIpUsuario(Integer ipUsuario) {
        this.ipUsuario = ipUsuario;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}
