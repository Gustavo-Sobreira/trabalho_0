package Ex_16042024;

public class TemaEscuroPequeno extends AplicativoDecorator{
    public TemaEscuroPequeno(IAplicativo aplicativo) {
        super(aplicativo);
    }

    @Override
    public String getTemaAtual() {
        return "Escuro";
    }

    @Override
    public Integer getZoom() {
        return 100;
    }
}
