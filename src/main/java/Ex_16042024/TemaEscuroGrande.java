package Ex_16042024;

public class TemaEscuroGrande extends AplicativoDecorator{
    public TemaEscuroGrande(IAplicativo aplicativo) {
        super(aplicativo);
    }

    @Override
    public String getTemaAtual() {
        return "Escuro";
    }

    @Override
    public Integer getZoom() {
        return 300;
    }
}
