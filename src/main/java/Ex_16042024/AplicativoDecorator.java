package Ex_16042024;

public abstract class AplicativoDecorator implements IAplicativo{

    private IAplicativo aplicativo;
    public String valor;

    public AplicativoDecorator(IAplicativo aplicativo){
        this.aplicativo = aplicativo;
    }

    public IAplicativo getAplicativo(){
        return aplicativo;
    }

    public void setAplicativo(IAplicativo aplicativo){
        this.aplicativo = aplicativo;
    }

    public abstract String getTemaAtual();
    @Override
    public String getCorPadrao() {
        return this.getTemaAtual() == "Claro" ? "Azul" : "Verde";
    }

    public abstract Integer getZoom();
    @Override
    public String getTamanhoFonte() {
        return this.getZoom() >= 200 ? "24px" : "12px";
    }
}
