package Ex_16042024;

public class Aplicacao implements IAplicativo{
    public String corPadrao;

    public Aplicacao(){}
    public Aplicacao(String corPadrao){
        this.corPadrao = corPadrao;
    }

    @Override
    public String getCorPadrao() {
        return corPadrao;
    }

    @Override
    public String getTamanhoFonte() {
        return "12px";
    }
}