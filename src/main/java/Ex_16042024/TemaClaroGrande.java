package Ex_16042024;

public class TemaClaroGrande extends AplicativoDecorator{
    public TemaClaroGrande(IAplicativo aplicativo) {
        super(aplicativo);
    }

    @Override
    public String getTemaAtual() {
        return "Claro";
    }

    @Override
    public Integer getZoom() {
        return 300;
    }
}
