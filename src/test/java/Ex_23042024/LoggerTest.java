package Ex_23042024;

import static org.junit.Assert.*;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LoggerTest {

    private Logger logger;

    @Before
    public void setUp() {
        logger = Logger.getInstance();
    }

    @After
    public void tearDown() {
        logger.destroyer();
    }

    @Test
    public void testLog() throws IOException {
        String logEsperado = "Trabalho 0 Log pra curioso é ...";
        logger.log(logEsperado);

        String ultimoLog = logger.ultimoLog("trabalho0padrao_singleton.log");
        assertTrue(ultimoLog.contains(logEsperado));
    }

    @Test
    public void testGetInstance() {
        Logger logger2 = Logger.getInstance();
        assertNotNull(logger2);
        assertSame(logger, logger2);
    }

    @Test
    public void testClose() {
        logger.fecharLogger();
        assertTrue(logger.fecharLogger());
    }

    @Test
    public void testDestroyer() {
        assertNull(logger.destroyer());
   }
}
