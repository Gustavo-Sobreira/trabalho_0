package Ex_29052024;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class ConfiguracaoTest {


    @Test
    void deveArmazenarEstados() {
        Configuracao configuracao = new Configuracao();
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaClaro.getInstance());
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaEscuro.getInstance());
        assertEquals(2, configuracao.getEstados().size());
    }

    @Test
    void deveRetornarEstadoInicial() {
        Configuracao configuracao = new Configuracao();
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaClaro.getInstance());
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaEscuro.getInstance());
        configuracao.voltarEstadoAnterior(0);
        assertEquals(ConfiguracaoEstadoTemaClaro.getInstance(), configuracao.getConfiguracao());
    }

    @Test
    void deveRetornarEstadoAnterior() {
        Configuracao configuracao = new Configuracao();
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaClaro.getInstance());
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaEscuro.getInstance());
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaEscuro.getInstance());
        configuracao.voltarEstadoAnterior(2);
        assertEquals(ConfiguracaoEstadoTemaEscuro.getInstance(), configuracao.getConfiguracao());
    }

    @Test
    void deveResetarConfiguracoes() {
        Configuracao configuracao = new Configuracao();
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaClaro.getInstance());
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaEscuro.getInstance());
        configuracao.setConfiguracao(ConfiguracaoEstadoTemaEscuro.getInstance());
        configuracao.resetarConfiguracao();
        assertEquals(ConfiguracaoEstadoTemaClaro.getInstance(), configuracao.getConfiguracao());
    }
}
