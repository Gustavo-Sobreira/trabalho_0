package Ex_17042024;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TransporteTerrestreTest {

    @Test
    void calcularCusto_DeveRetornarCustoCorretoParaDistancia() {
        TransporteTerrestre transporte = new TransporteTerrestre();
        double distancia = 500;
        double custoEsperado = distancia * 0.1;
        Assertions.assertEquals(custoEsperado, transporte.calcularCusto(distancia));
    }
}