package Ex_17042024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TransporteTerrestreFactoryTest {

    @Test
    void criarTransporte_DeveRetornarInstanciaDeTransporteTerra() {
        TransporteFactory factory = new TransporteTerrestreFactory();
        ITransporte transporte = factory.criarTransporte();
        assertTrue(transporte instanceof TransporteTerrestre);
    }
}
