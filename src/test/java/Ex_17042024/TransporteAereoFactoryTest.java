package Ex_17042024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class  TransporteAereoFactoryTest {

    @Test
    void criarTransporte_DeveRetornarInstanciaDeTransporteAereo() {
        TransporteFactory factory = new TransporteAereoFactory();
        ITransporte transporte = factory.criarTransporte();
        assertTrue(transporte instanceof TransporteAereo);
    }
}
