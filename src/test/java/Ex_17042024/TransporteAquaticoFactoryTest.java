package Ex_17042024;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TransporteAquaticoFactoryTest {

    @Test
    public void criarTransporte_DeveRetornarInstanciaDeTransporteAgua() {
        TransporteFactory factory = new TransporteAquaticoFactory();
        ITransporte transporte = factory.criarTransporte();
        assertTrue(transporte instanceof TransporteAquatico);
    }
}
