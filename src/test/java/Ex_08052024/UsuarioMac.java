package Ex_08052024;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UsuarioMac {
    String diretorioAtual = "Área_de_Trabalho Conhecimentos Downloads Faculdade Profissional snap Certificados Cursos eBooks Imagens senhas TCC Documentos Estudos Monitoria";

    @Test
    public void deveRetornarTodosDiretoriosDaMaquina(){
        IMaquina maquina = new MaquinaMac();
        String retorno = maquina.receberComando("ls");

        assertEquals(retorno, diretorioAtual);
    }

    @Test
    public void deveNavegarAteUmDiretorio(){
        IMaquina maquina = new MaquinaMac();
        String retorno = maquina.receberComando("cd snap");

        assertEquals(retorno, "Navegando para o diretório: snap");
    }



}
