package Ex_16042024;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class AplicativoTest {
    @Test
    public void deveUsarCorAzulCom12px(){
        IAplicativo aplicativo = new Aplicacao("Azul");
        assertEquals("Azul", aplicativo.getCorPadrao());
        assertEquals("12px", aplicativo.getTamanhoFonte());
    }

    @Test
    public void deveUsarCorAzulCom24px(){
        IAplicativo aplicativo = new TemaClaroGrande(new Aplicacao());

        assertEquals("Azul", aplicativo.getCorPadrao());
        assertEquals("24px", aplicativo.getTamanhoFonte());
    }

    @Test
    public void deveUsarCorVerdeCom24px(){
        IAplicativo aplicativo = new TemaEscuroGrande(new Aplicacao());

        assertEquals("Verde", aplicativo.getCorPadrao());
        assertEquals("24px", aplicativo.getTamanhoFonte());
    }

    @Test
    public void deveUsarCorVerdeCom12px(){
        IAplicativo aplicativo = new TemaEscuroPequeno(new Aplicacao());

        assertEquals("Verde", aplicativo.getCorPadrao());
        assertEquals("12px", aplicativo.getTamanhoFonte());
    }

}
