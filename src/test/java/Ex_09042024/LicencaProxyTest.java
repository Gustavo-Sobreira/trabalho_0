package Ex_09042024;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LicencaProxyTest {

    @Test
    void deveFuncionarCorretamente() {
        LicencaProxy proxy = new LicencaProxy(new Software(123, "Software gráfico"));
        Empresa empresa = new Empresa(3);
        Acesso usuario1 = new Acesso(123,1, empresa);
        Acesso usuario2 = new Acesso(123,2, empresa);
        Acesso usuario3 = new Acesso(123,3, empresa);

        proxy.interagir(usuario1);
        proxy.interagir(usuario2);
        assertEquals("Usuário 3 interagindo com Software gráfico", proxy.interagir(usuario3));
    }

    @Test
    void deveColocarUsuarioNaFila() {
        LicencaProxy proxy = new LicencaProxy(new Software(123, "Software gráfico"));
        Empresa empresa = new Empresa(2);
        Acesso usuario1 = new Acesso(123,1, empresa);
        Acesso usuario2 = new Acesso(123,2, empresa);
        Acesso usuario3 = new Acesso(123,3, empresa);

        proxy.interagir(usuario1);
        proxy.interagir(usuario2);
        assertEquals("[OPS] - Você está na fila de acesso, aguarde ou compre mais licenças", proxy.interagir(usuario3));
    }

    @Test
    void deveLiberarFilaParaNovoUsuario() {
        LicencaProxy proxy = new LicencaProxy(new Software(123, "Software gráfico"));
        Empresa empresa = new Empresa(2);
        Acesso usuario1 = new Acesso(123,1, empresa);
        Acesso usuario2 = new Acesso(123,2, empresa);
        Acesso usuario3 = new Acesso(123,3, empresa);

        proxy.interagir(usuario1);
        proxy.interagir(usuario2);
        assertEquals("[OPS] - Você está na fila de acesso, aguarde ou compre mais licenças", proxy.interagir(usuario3));
        System.out.printf("Antes:");
        System.out.println(proxy.listarAcessos());
        proxy.liberarAcesso(usuario2);
        System.out.printf("Depois:");
        System.out.printf(proxy.listarAcessos());
    }
}
