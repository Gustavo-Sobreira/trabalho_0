package Ex_10042024;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FilterFactoryTest {

    @Test
    void getFiltro_DeveRetornarFiltroGaussianBlur_QuandoTipoEhGaussianBlur() {
        Filtro filtro = FilterFactory.getFiltro("gaussianBlur");
        assertTrue(filtro instanceof GaussianBlurFilter);
    }

    @Test
    void getFiltro_DeveRetornarFiltroSepia_QuandoTipoEhSepia() {
        Filtro filtro = FilterFactory.getFiltro("sepia");
        assertTrue(filtro instanceof SepiaFilter);
    }

    @Test
    void getFiltro_DeveRetornarMesmaInstancia_QuandoChamadoDuasVezesComMesmoTipo() {
        Filtro filtro1 = FilterFactory.getFiltro("gaussianBlur");
        Filtro filtro2 = FilterFactory.getFiltro("gaussianBlur");
        assertSame(filtro1, filtro2);
    }
}

