package Ex_10042024;

import org.junit.jupiter.api.Test;

public class ImagemTest {

    @Test
    void aplicarFiltro_DeveAplicarFiltroGaussianBlur() {
        Imagem imagem = new Imagem("foto.jpg");
        String tipoFiltro = "gaussianBlur";
        imagem.aplicarFiltro(tipoFiltro);
    }

    @Test
    void aplicarFiltro_DeveAplicarFiltroSepia() {
        Imagem imagem = new Imagem("foto.jpg");
        String tipoFiltro = "sepia";
        imagem.aplicarFiltro(tipoFiltro);
    }
}
