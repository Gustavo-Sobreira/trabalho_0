# Trabalho 0 - Panorama geral das atividades
GRUPO: 3 
- Gustavo Sobreira
- Matheus Polesca
- Rodrigo Bento de Macedo
### Requisitos
- JDK: 21
- Maven

A divisão do projeto foi feita atravéz da data de aplicação das atividades, seguindo o seguinte padrão:
`Ex_ddmmaaaa`

## 09/04/2024 - Proxy
#### Diagrama
![image.png](src/docs/Ex_09042024/image.png)
#### Teste
![image.png](src/docs/Ex_09042024/teste.png)
***

## 10/04/2024 - Flyweight
#### Diagrama
![image.png](src/docs/Ex_10042024/Diagrama.png)
#### Teste
![image.png](src/docs/Ex_10042024/img.png)
***

## 16/04/2024 - Decorator
#### Diagrama
![image.png](src/docs/Ex_16042024/image.png)
#### Teste
![image.png](src/docs/Ex_16042024/teste.png)
***

## 17/04/2024 - Factory Method
#### Diagrama
![image.png](src/docs/Ex_17042024/image.png)
#### Teste
![image.png](src/docs/Ex_17042024/teste.png)
***

## 23/04/2024 - Singleton
#### Diagrama
![image.png](src/docs/Ex_23042024/Diagrama.png)
#### Teste
![image.png](src/docs/Ex_23042024/img.png)
***

## 08/05/2024 - Interpreter
#### Diagrama
![image.png](src/docs/Ex_08052024/img.png)
#### Teste
![image.png](src/docs/Ex_08052024/teste.png)
***

## 29/05/2024 - Memento
#### Diagrama
![image.png](src/docs/Ex_29052024/image.png)
#### Teste
![image.png](src/docs/Ex_29052024/teste.png)
***